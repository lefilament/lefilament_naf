# © 2011 Numérigraphe SARL
# © 2019 Le Filament
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'French NAF codes',
    'version': '12.0.2.0.0',
    'author': u'Numérigraphe SARL,'
              u'Le Filament,'
              u'Odoo Community Association (OCA)',
    'category': 'French Localization',
    'depends': ['contacts'],
    'data': [
        "security/ir.model.access.csv",
        'data/res.partner.naf.csv',
        'views/res_partner.xml'],
    'license': 'AGPL-3',
}
