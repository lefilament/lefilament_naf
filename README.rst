.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

==========================================
French NAF partner categories and APE code
==========================================

This module imports the French official NAF
nomenclature of partner activities as partner categories.
This module is based on the one from OCA but removing complexity of importing from EU NACE codes

It will also add a field to the partner form, to enter the partner's APE
(official main activity of the company), picked among the NAF nomenclature.

Usage
=====

A new tables called Codes NAF is created and added to Contacts / Configuration menu
A new field is added on partner form called NAF code where you can pick one of the code from previous table

Credits
=======

Contributors
------------

* Lionel Sausin (Numérigraphe) <ls@numerigraphe.com>
* Rémi Cazenave (Le Filament) <remi@le-filament.com>

Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
