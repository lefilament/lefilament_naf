# © 2019 Le Filament
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import models, fields


class ResPartnerNaf(models.Model):
    _name = "res.partner.naf"
    _description = "Codes NAF"

    name = fields.Char('Code NAF')
