# © 2019 Le Filament
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import models, fields


class ResPartner(models.Model):
    """Add the French APE (official main activity of the company)"""

    _inherit = 'res.partner'

    naf_id = fields.Many2one(
        'res.partner.naf',
        string='Code NAF',
    )
